package com.excendia.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class Utils {

    public static ResponseEntity<String> createResponseEntity(HttpStatus status, String msg) {
        return new ResponseEntity<>(msg, status);
    }

}
