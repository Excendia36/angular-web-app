package com.excendia.utils;

public class TemplateMessages {

    public static final String TEMPLATE_IS_NOT_A_VALID_EMAIL_MESSAGE = "'%s' muss eine richtige Email sein!";
    public static final String TEMPLATE_IS_NOT_A_VALID_PASSWORD_MESSAGE = "Das Passwort muss 8 Zeichen und eine Zahl enthalten!";
    public static final String TEMPLATE_EMAIL_ALREADY_IN_USE_MESSAGE = "Die Email '%s' wird bereits genutzt!";
    public static final String TEMPLATE_SUCCESSFUL_SIGN_UP_MESSAGE = "Ein neuer Account wurde erstellt mit der Email '%s'!";
    public final static String TEMPLATE_USER_NOT_FOUND_MESSAGE = "Ein Account mit der Email '%s' existiert nicht!";

}
