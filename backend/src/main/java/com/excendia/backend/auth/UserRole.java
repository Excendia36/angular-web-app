package com.excendia.backend.auth;

public enum UserRole {
    USER,
    ADMIN
}
