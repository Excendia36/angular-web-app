package com.excendia.backend.auth.login;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(path = "api/v1/login")
@AllArgsConstructor
public class LoginController {

    private LoginService loginService;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @PostMapping
    public ResponseEntity<String> register(@RequestBody LoginRequest loginRequest) {
        logger.info("Registration: Initiating registration for user '" + loginRequest.getEmail() + "'");
        return loginService.login(loginRequest);
    }
}
