package com.excendia.backend.auth.login;

import com.excendia.backend.auth.registration.RegistrationRequest;
import com.excendia.backend.entity.AppUser;
import com.excendia.backend.auth.UserRole;
import com.excendia.backend.auth.UserService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class LoginService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final UserService userService;

    public ResponseEntity<String> register(RegistrationRequest request) {

        ResponseEntity<String> responseEntity = userService.singUpUser(
                new AppUser(
                        request.getFirstName(),
                        request.getLastName(),
                        request.getEmail(),
                        request.getPassword(),
                        UserRole.USER));
        return responseEntity;
    }


    public ResponseEntity<String> login(LoginRequest loginRequest) {

        ResponseEntity<String> responseEntity = userService.loginUser(loginRequest.getEmail(), loginRequest.getPassword());
        return responseEntity;
    }
}
