package com.excendia.backend.auth.registration;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(path = "api/v1/registration")
@AllArgsConstructor
public class RegistrationController {

    private RegistrationService registrationService;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @PostMapping
    public ResponseEntity<String> register(@RequestBody RegistrationRequest registrationRequest) {
        logger.info("Registration: Initiating registration for user '" + registrationRequest.getEmail() + "'");
        return registrationService.register(registrationRequest);
    }
}
