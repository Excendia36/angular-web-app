package com.excendia.backend.auth.registration;

import com.excendia.backend.auth.EmailValidator;
import com.excendia.backend.auth.PasswordValidator;
import com.excendia.backend.entity.AppUser;
import com.excendia.backend.auth.UserRole;
import com.excendia.backend.auth.UserService;
import com.excendia.utils.TemplateMessages;
import com.excendia.utils.Utils;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RegistrationService {
    private final EmailValidator emailValidator;
    private final PasswordValidator passwordValidator;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final UserService userService;

    public ResponseEntity<String> register(RegistrationRequest request) {
        boolean isValidEmail = emailValidator.validate(request.getEmail());
        if (!isValidEmail) {
            logger.info(String.format(TemplateMessages.TEMPLATE_IS_NOT_A_VALID_EMAIL_MESSAGE, request.getEmail()));
            return Utils.createResponseEntity(HttpStatus.BAD_REQUEST, String.format(TemplateMessages.TEMPLATE_IS_NOT_A_VALID_EMAIL_MESSAGE, request.getEmail()));
        }

        boolean isValidPassword = passwordValidator.validate(request.getPassword());
        if (!isValidPassword) {
            logger.info(TemplateMessages.TEMPLATE_IS_NOT_A_VALID_PASSWORD_MESSAGE);
            return Utils.createResponseEntity(HttpStatus.BAD_REQUEST, TemplateMessages.TEMPLATE_IS_NOT_A_VALID_PASSWORD_MESSAGE);
        }

        ResponseEntity<String> responseEntity = userService.singUpUser(
                new AppUser(
                        request.getFirstName(),
                        request.getLastName(),
                        request.getEmail(),
                        request.getPassword(),
                        UserRole.USER));
        return responseEntity;
    }


}
