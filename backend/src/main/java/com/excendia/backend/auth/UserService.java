package com.excendia.backend.auth;

import com.excendia.backend.entity.AppUser;
import com.excendia.backend.repository.UserRepository;
import com.excendia.utils.TemplateMessages;
import com.excendia.utils.Utils;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class UserService implements UserDetailsService {


    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return userRepository.findByEmail(email).
                orElseThrow(() ->
                        new UsernameNotFoundException(String.format(TemplateMessages.TEMPLATE_USER_NOT_FOUND_MESSAGE, email)));
    }

    public ResponseEntity<String> singUpUser(AppUser appUser) {
        boolean userExists = userRepository.findByEmail(appUser.getEmail()).isPresent();
        if (userExists) {
            return Utils.createResponseEntity(HttpStatus.BAD_REQUEST, String.format(TemplateMessages.TEMPLATE_EMAIL_ALREADY_IN_USE_MESSAGE, appUser.getEmail()));
        }
        String encodedPassword = bCryptPasswordEncoder.encode(appUser.getPassword());
        appUser.setPassword(encodedPassword);
        userRepository.save(appUser);
        ResponseEntity<String> res = Utils.createResponseEntity(HttpStatus.OK, String.format(TemplateMessages.TEMPLATE_SUCCESSFUL_SIGN_UP_MESSAGE, appUser.getEmail()));
        logger.info(res.getBody());
        return res;
    }

    public ResponseEntity<String> loginUser(String email, String password){
        //TODO: Login user logic
        return null;
    }

    public int enableAppUser(String email) {
        return userRepository.enableAppUser(email);
    }
}
