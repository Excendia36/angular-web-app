import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http: HttpClient) {
  }

  registerUser(form: FormGroup) {
    return this.http.post("http://localhost:8080/api/v1/registration", form.getRawValue(), {responseType: 'text'});
  }
}
