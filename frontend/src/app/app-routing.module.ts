import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArbeitsstundenComponent } from './arbeitsstunden/arbeitsstunden.component';
import { LoginComponent } from './auth/login/login.component';
import { EinkaufslisteNavbarComponent } from './einkaufsliste-navbar/einkaufsliste-navbar.component';
import { EinkaufslisteComponent } from './einkaufsliste-navbar/einkaufsliste/einkaufsliste.component';
import { EssensplanComponent } from './essensplan/essensplan.component';
import { HomeComponent } from './home/home.component';
import { StammdatenComponent } from './stammdaten/stammdaten.component';
import { RegisterComponent } from './auth/register/register.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'essensplan', component: EssensplanComponent },
  { path: 'stammdaten', component: StammdatenComponent },
  { path: 'arbeitsstunden', component: ArbeitsstundenComponent },
  { path: 'einkaufsliste', component: EinkaufslisteComponent },
  { path: 'home', component: HomeComponent },
  { path: 'auth/login', component: LoginComponent },
  { path: 'auth/register', component: RegisterComponent }
];

@NgModule({
  imports: [CommonModule,
    RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
