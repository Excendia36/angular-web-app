import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArbeitsstundenComponent } from './arbeitsstunden.component';

describe('ArbeitsstundenComponent', () => {
  let component: ArbeitsstundenComponent;
  let fixture: ComponentFixture<ArbeitsstundenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArbeitsstundenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArbeitsstundenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
