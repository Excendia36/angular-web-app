import { Zutat } from "../interfaces/zutat";

export const ZUTATEN: Zutat[] = [
    { id: 1, name: "Apfel" },
    { id: 2, name: "Banane" },
    { id: 3, name: "Zitrone" },
    { id: 4, name: "Brot" },
    { id: 5, name: "Kiwi" },
    { id: 6, name: "Kartoffel" },
    { id: 7, name: "Zwiebel" },
    { id: 8, name: "Orange" },
]

export const EMPTY_ZUTATEN: Zutat[] = []


export const ZUTATEN2: Zutat[] = [
    { id: 9, name: "Gapfel" },
    { id: 10, name: "Kranane" },
    { id: 11, name: "Britrone" },
    { id: 12, name: "Torot" },
    { id: 13, name: "Skiwi" },
    { id: 14, name: "Blartoffel" },
    { id: 15, name: "Wuwiebel" },
    { id: 16, name: "Lorange" },
]

export const ALLE_ZUTATEN: Zutat[] = [
    { id: 1, name: "Apfel" },
    { id: 2, name: "Banane" },
    { id: 3, name: "Zitrone" },
    { id: 4, name: "Brot" },
    { id: 5, name: "Kiwi" },
    { id: 6, name: "Kartoffel" },
    { id: 7, name: "Zwiebel" },
    { id: 8, name: "Orange" },
    { id: 9, name: "Gapfel" },
    { id: 10, name: "Kranane" },
    { id: 11, name: "Britrone" },
    { id: 12, name: "Torot" },
    { id: 13, name: "Skiwi" },
    { id: 14, name: "Blartoffel" },
    { id: 15, name: "Wuwiebel" },
    { id: 16, name: "Lorange" },
]

