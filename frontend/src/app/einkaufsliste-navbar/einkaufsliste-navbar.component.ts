import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ALLE_ZUTATEN, EMPTY_ZUTATEN } from '../mock-data/mock-zutat';


@Component({
  selector: 'app-einkaufsliste-navbar',
  templateUrl: './einkaufsliste-navbar.component.html',
  styleUrls: ['./einkaufsliste-navbar.component.scss']
})
export class EinkaufslisteNavbarComponent implements OnInit {

  constructor() { }

  empty_zutaten = EMPTY_ZUTATEN
  all_zutaten = ALLE_ZUTATEN
  zutaten = new FormControl();

  ngOnInit(): void {
  }


  onSearchTermChange(): void {
  }

}
