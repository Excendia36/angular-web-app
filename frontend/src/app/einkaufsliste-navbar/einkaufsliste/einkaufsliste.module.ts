import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EinkaufslisteNavbarComponent } from '../einkaufsliste-navbar.component';
import { BrowserModule } from '@angular/platform-browser';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { EinkaufslisteComponent } from './einkaufsliste.component';



@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    BrowserModule,
    MDBBootstrapModule
  ],
})
export class EinkaufslisteModule { }
