import { Component, OnInit } from '@angular/core';
import { ALLE_ZUTATEN, EMPTY_ZUTATEN, ZUTATEN, ZUTATEN2 } from 'src/app/mock-data/mock-zutat';

@Component({
  selector: 'app-einkaufsliste',
  templateUrl: './einkaufsliste.component.html',
  styleUrls: ['./einkaufsliste.component.scss']
})
export class EinkaufslisteComponent implements OnInit {

  zutaten = ZUTATEN;
  zutaten2 = ZUTATEN2;
  all_zutaten = ALLE_ZUTATEN
  empty_zutaten = EMPTY_ZUTATEN


  constructor() { }

  ngOnInit(): void {
  }

}
