interface User{
    firstname: String,
    lastname: String,
    email: String,
    password: String
}

interface UserLogin{
    email: String,
    password: String
}